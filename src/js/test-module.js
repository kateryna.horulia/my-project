import {additionalUsers, randomUserMock} from "./mock";
import {findUser, formatObjectsArrays, sortArray, filterArray, validateObject} from "./script";

const array = formatObjectsArrays(randomUserMock, additionalUsers);
// console.log(validateObject(array[0]));
// console.log((array[0]));

const paramObj = {
    country: 'Germany',
    age: 65
}

const paramObj2 = {
    country: 'Germany',
    gender: 'female'
}

const paramObj3 = {
    full_name: 'Norbert Weishaupt',
    note: null,
    age: 65
}

// console.log(filterArray(array, paramObj));
// console.log(filterArray(array, paramObj3));

// console.log(sortArray(array, 'b_day', 'decrease'));

const paramObj4 = {
    age: 65
}

console.log(findUser(array, paramObj4));
