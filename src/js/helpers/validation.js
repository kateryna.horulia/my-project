const email_reg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const phone_reg = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/; // +(123)-456-78-90

export const validateString = (str) => {
    if(!str) return false;
    return typeof str === 'string' && str.charAt(0) === str.charAt(0).toUpperCase();
}

export const validateAge = (age) => {
    if(!age) return false;
    return typeof age === 'number';
}

export const validatePhone = (phone) => {
    if(!phone) return false;
    return phone_reg.test(phone);
}

export const validateMail = (mail) => {
    if(!mail) return false;
    return email_reg.test(mail);
}
