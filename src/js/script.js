import { additionalUsers, randomUserMock } from './mock.js';
import { validateString, validateAge, validatePhone, validateMail } from "./helpers/validation.js";

function generateId() {
    return Math.random().toString(16).slice(2);
}

const array = formatObjectsArrays(randomUserMock, additionalUsers);

export function formatObjectsArrays(randomUserMock, additionalUsers) {
    const formattedUserMock = randomUserMock.map((obj) => {
        return {
            gender: obj.gender,
            title: obj.name.title,
            full_name: `${obj.name.first} ${obj.name.last}`,
            city: obj.location.city,
            state: obj.location.state,
            country: obj.location.country,
            postcode: obj.location.postcode,
            coordinates: {
                latitude: obj.location.coordinates.latitude,
                longitude: obj.location.coordinates.longitude,
            },
            timezone: {
                offset: obj.location.timezone.offset,
                description: obj.location.timezone.description,
            },
            email: obj.email,
            b_date: obj.dob.date,
            age: obj.dob.age,
            phone: obj.phone,
            picture_large: obj.picture.large,
            picture_thumbnail: obj.picture.thumbnail,
            id: generateId(),
            favorite: obj.favorite? obj.favorite : null,
            course: obj.course? obj.course : null,
            bg_color: obj.bg_color? obj.bg_color : null,
            note: obj.note? obj.note : null,
        };
    })

    const fullNamesArray = [];
    const concatenateArray = [...formattedUserMock, ...additionalUsers];

    const finalArray = [];

    for (let i=0; i < concatenateArray.length ; i++){
        if (!(fullNamesArray.indexOf(concatenateArray[i].full_name) > -1)) {
            finalArray.push(concatenateArray[i]);
            fullNamesArray.push(concatenateArray[i].full_name);
        }
    }
    return finalArray;
}

export function validateObject(object) {
    const errors = {
        full_name: validateString(object.full_name),
        gender: validateString(object.gender),
        note:  validateString(object.note),
        state: validateString(object.state),
        city: validateString(object.city),
        country: validateString(object.country),
        age: validateAge(object.age),
        phone: validatePhone(object.phone),
        email: validateMail(object.email)
    };

    if (Object.values(errors).every(item => item === true)) {
        return 'Object is valid'
    }
    return 'Object is not valid. ' + JSON.stringify(errors);
}

export function filterArray(array, paramObj) {

    const filteredArray = array.filter((obj) => {
        for (let key in paramObj) {
            if (obj[key] === undefined || obj[key] != paramObj[key]) {
                return false;
            }
        }
        return true;
    })

    return filteredArray;
}

export function filterAge(array, age, isOlder) {

    const filteredArray = array.filter((obj) => isOlder? obj.age > age : obj.age < age)

    return filteredArray;
}

export function sortArray(array, param, order) {
    switch(param) {
        case 'full_name':
            if(order === 'decrease') {
                return array.sort((a, b) => (a.full_name.toLowerCase() < b.full_name.toLowerCase()) ? 1 : -1);
            }
            return array.sort((a, b) => (a.full_name.toLowerCase() > b.full_name.toLowerCase()) ? 1 : -1);
        case 'country':
            if(order === 'decrease') {
                return array.sort((a, b) => (a.country.toLowerCase() < b.country.toLowerCase()) ? 1 : -1);
            }
            return array.sort((a, b) => (a.country.toLowerCase() > b.country.toLowerCase()) ? 1 : -1);
        case 'age':
            if(order === 'decrease') {
                return array.sort((a, b) => (b.age - a.age));
            }
            return array.sort((a, b) => (a.age - b.age));
        case 'b_day':
            if(order === 'decrease') {
                return array.sort((a, b) => new Date(b.b_date) - new Date(a.b_date));
            }
            return array.sort((a, b) => new Date(a.b_date) - new Date(b.b_date));
        default:
            return array;
    }
}

export function findUser(array, paramObj) {
    const user = array.find((obj) => {
        for (let key in paramObj) {
            if (obj[key] === undefined || obj[key] != paramObj[key]) {
                return false;
            }
        }
        return true;
    })

    return user;
}


export function getPercentage(array, searchedArray) {
    return  Math.floor((searchedArray.length / array.length) * 100);
}

const paramObj4 = {
    age: 65
}

const filteredArray = filterArray(array, paramObj4);
const ageArray = filterAge(array, 30, false);
console.log(getPercentage(array,ageArray));
