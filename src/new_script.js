let array = [];
let array_10 = [];

async function onStart() {
  const teachersResponse = await fetch('https://randomuser.me/api/?results=50');
  const teachersData = await teachersResponse.json();
  array = formatObjectsArrays(teachersData.results);
  renderTeachers(array);
  renderFavTeachers(array);

  const teachersResponse_10 = await fetch('https://randomuser.me/api/?results=10');
  const teachersData_10 = await teachersResponse_10.json();
  array_10 = formatObjectsArrays(teachersData_10.results);
  renderStatistics(array_10);
}

onStart()
  .catch(error => alert(error.message));

document.getElementById("modal_add_teacher_form").addEventListener("submit", function(event){
  event.preventDefault()
});

async function showMoreTeachers() {
  const teachersResponse_10 = await fetch('https://randomuser.me/api/?results=10');
  const teachersData_10 = await teachersResponse_10.json();
  array_10 = formatObjectsArrays(teachersData_10.results);
  clearStatistics()
  renderStatistics(array_10);
}

function sortAge() {
  let sortedArray = [];
  const age_th = document.getElementById("statistics_age_th");
  age_th.classList.toggle("increase");

  if(age_th.classList.contains("increase")) {
    sortedArray = sortArray(array_10, 'age', 'increase');
  } else sortedArray = sortArray(array_10, 'age', 'decrease');


  clearStatistics();
  renderStatistics(sortedArray);
}

function sortName() {
  let sortedArray = [];
  const full_name_th = document.getElementById("statistics_full_name_th");
  full_name_th.classList.toggle("increase");

  if(full_name_th.classList.contains("increase")) {
    sortedArray = sortArray(array_10, 'full_name', 'increase');
  } else sortedArray = sortArray(array_10, 'full_name', 'decrease');


  clearStatistics();
  renderStatistics(sortedArray);
}

function sortCountry() {
  let sortedArray = [];
  const country_th = document.getElementById("statistics_country_th");
  country_th.classList.toggle("increase");

  if(country_th.classList.contains("increase")) {
    sortedArray = sortArray(array_10, 'country', 'increase');
  } else sortedArray = sortArray(array_10, 'country', 'decrease');


  clearStatistics();
  renderStatistics(sortedArray);
}

function searchTeacher() {
  const value = document.getElementById("header_form-input").value;

  const paramObj = {
    full_name: value,
  }
  const teacher = findUser(array, paramObj);
  toggleModal(teacher);
}

function toggleModal(teacher) {
  const teacher_modal = document.getElementById('teacher_modal');
  teacher_modal.classList.toggle("show_teacher_modal");

  const div_wrapper = document.createElement("div");
  div_wrapper.className = 'teacher_modal_content';
  div_wrapper.innerHTML =
    `
            <div class="teacher_modal_close" onClick="closeModal()">&times;</div>
            <div class="teacher_modal_title">Teacher Info</div>
            <div class="teacher_modal_inner">
                <img class="teacher_modal_img" src="${teacher.picture_large || 'images/default-image.jpeg'}">
                <div class="teacher_modal_text">
                <div class="teacher_modal_header" id="teacher_modal_header">
                   <div class="teacher_modal_name">${teacher.full_name}</div>
                </div>
                <p>${teacher.city}, ${teacher.country}</p>
                <p>${teacher.age}, ${teacher.gender}</p>
                <p class="email">${teacher.email}</p>
                <p>${teacher.phone}</p>
                </div>           
            </div>
            <div class="teacher_modal_footer">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                ea commodo consequat.
            </div>
            <a class="teacher_modal_toggle" id="teacher_modal_toggle">Add to favourite</a>
        `
  teacher_modal.appendChild(div_wrapper);

  if(teacher.favorite === true) {
    const teacher_header = document.getElementById("teacher_modal_header");
    const img = document.createElement('div');
    img.innerHTML = ` <img class="teacher_modal_star" src="images/star.png">`
    teacher_header.appendChild(img);
  }
  document.getElementById("teacher_modal_toggle").addEventListener('click', function(){addToFav(teacher)});
}

function closeModal() {
  document.getElementById('teacher_modal').classList.toggle("show_teacher_modal");
  document.getElementById('teacher_modal').innerHTML = '';
}

function addToFav(teacher) {
  const user = findUser(array, {id: teacher.id});
  user.favorite = true;
  closeModal();
  toggleModal(teacher);

  clearTeachers();
  renderTeachers(array);

  clearFavTeachers();
  renderFavTeachers(array);
}

async function filterTeachers() {
  const country = document.getElementById("filter_teacher_country").value;
  const age = document.getElementById("filter_teacher_age").value;
  const gender = document.getElementById("filter_teacher_gender").value;
  const favorite = document.getElementById("filter_teacher_fav").checked;

  if(gender && !country && !age && !favorite) {
    const teachersResponse = await fetch(`https://randomuser.me/api/?results=50&gender=${gender}`);
    const teachersData = await teachersResponse.json();
    array = formatObjectsArrays(teachersData.results);
    clearTeachers();
    renderTeachers(array);
    return;
  }

  if(!gender && !country && age && !favorite) {
    const teachersResponse = await fetch(`https://randomuser.me/api/?results=50&age=${age}`);
    const teachersData = await teachersResponse.json();
    array = formatObjectsArrays(teachersData.results);
    clearTeachers();
    renderTeachers(array);
    return;
  }

  if(!gender && country && !age && !favorite) {
    const teachersResponse = await fetch(`https://randomuser.me/api/?results=50&nat=${country}`);
    const teachersData = await teachersResponse.json();
    array = formatObjectsArrays(teachersData.results);
    clearTeachers();
    renderTeachers(array);
    return;
  }

  const arr = [{'country': country} , {'age': age}, {'gender': gender}, {'favorite': favorite}];
  const existArr = [];
  const paramObj = {};

  arr.forEach(el => {
    for(let key in el) {
      if(el[key] === undefined || el[key] === '' || el[key] === false) return;
      else existArr.push(el);
    }
  })

  existArr.forEach(el => {
    for(let key in el) {
      paramObj[key] = el[key];
    }
  })

  const filteredArray = filterArray(array, paramObj);
  clearTeachers();
  renderTeachers(filteredArray);
}

function openTeacherModal() {
  document.getElementById("modal").style.display = 'flex';
}

function closeTeacherModal() {
  document.getElementById("modal").style.display = 'none';
}

async function addTeacher() {
  const name = document.getElementById("add_teacher_name").value;
  const country = document.getElementById("add_teacher_country").value;
  const city = document.getElementById("add_teacher_city").value;
  const phone = document.getElementById("add_teacher_phone").value;
  const email = document.getElementById("add_teacher_email").value;
  const age = parseInt(document.getElementById("add_teacher_age").value);
  const color = document.getElementById("add_teacher_color").value;
  const female = document.getElementById("add_teacher_female").checked;
  const male = document.getElementById("add_teacher_male").checked;

  let gender = '';
  if(female) {
    gender = 'Female';
  } else if(male) {
    gender = 'Male';
  }

  const newTeacher = {
    gender: gender,
    full_name: name,
    city: city,
    state: 'Default',
    country: country,
    email: email,
    age: age,
    phone: phone,
    id: generateId(),
    favorite: false,
    bg_color: color,
    note: 'Default',
  }
  console.log(newTeacher);

  if(validateObject(newTeacher) === true) {
    await fetch('http://localhost:3000/posts', {
      method: 'POST',
      body: JSON.stringify(newTeacher),
      headers: { 'Content-Type': 'application/json' },
    });

    clearTeachers();
    renderTeachers(array);
    closeTeacherModal();
  } else alert('User is incorrect! Try again.')
}

// RENDER FUNCTIONS //
function renderStatistics(array) {
  const stat_wrapper = document.getElementById("statistics_tbody");
  array.forEach(teacher => {
    const tr_wrapper = document.createElement("tr");
    tr_wrapper.innerHTML =
      `
              <td>${teacher.full_name}</td>
              <td>${teacher.age}</td>
              <td>${teacher.gender}</td>
              <td>${teacher.country}</td>
        `
    stat_wrapper.appendChild(tr_wrapper);
  })
}

function clearStatistics() {
  document.getElementById("statistics_tbody").innerHTML = '';
}

function renderFavTeachers(array) {
  const fav_wrapper = document.getElementById("favorites_wrapper");
  const fav_array = array.filter(el => el.favorite === true);
  fav_array.forEach(teacher => {
    const div_wrapper = document.createElement("div");
    div_wrapper.className = 'favorites_item';
    div_wrapper.innerHTML =
      `
          <div class="favorites_item">
              <img class="favorites_image" src=${teacher.picture_large || 'images/default-image.jpeg'} alt="" />
              <div class="favorites_title">
                ${teacher.full_name}
              </div>
              <div class="favorites_country">
                ${teacher.country}
              </div>
          </div>
        `
    fav_wrapper.appendChild(div_wrapper);
  })
}

function clearFavTeachers() {
  document.getElementById("favorites_wrapper").innerHTML = '';
}

function renderTeachers(array) {
  const teachers_wrapper = document.getElementById("teachers_wrapper");
  array.forEach(teacher => {
    const div_wrapper = document.createElement("div");
    div_wrapper.className = 'teachers_item';
    if(teacher.favorite === true) {
      div_wrapper.classList.add('teachers_item-star');
    }
    div_wrapper.innerHTML =
      `
          <img class="favorites_image" src=${teacher.picture_large || 'images/default-image.jpeg'} alt="" />
          <div class="favorites_title">
            ${teacher.full_name}
          </div>
          <div class="favorites_country">
           ${teacher.country}
          </div>
        `
    div_wrapper.addEventListener('click', function() {toggleModal(teacher)});
    teachers_wrapper.appendChild(div_wrapper);
  })
}

function clearTeachers() {
  document.getElementById("teachers_wrapper").innerHTML = '';
}

// HELPERS FUNCTIONS //

function formatObjectsArrays(randomUserMock, additionalUsers) {
  const formattedUserMock = randomUserMock.map((obj) => {
    return {
      gender: obj.gender,
      title: obj.name.title,
      full_name: `${obj.name.first} ${obj.name.last}`,
      city: obj.location.city,
      state: obj.location.state,
      country: obj.location.country,
      postcode: obj.location.postcode,
      coordinates: {
        latitude: obj.location.coordinates.latitude,
        longitude: obj.location.coordinates.longitude,
      },
      timezone: {
        offset: obj.location.timezone.offset,
        description: obj.location.timezone.description,
      },
      email: obj.email,
      b_date: obj.dob.date,
      age: obj.dob.age,
      phone: obj.phone,
      picture_large: obj.picture.large,
      picture_thumbnail: obj.picture.thumbnail,
      id: generateId(),
      favorite: obj.favorite? obj.favorite : false,
      course: obj.course? obj.course : null,
      bg_color: obj.bg_color? obj.bg_color : null,
      note: obj.note? obj.note : null,
    };
  })
  return formattedUserMock;
}

function generateId() {
  return Math.random().toString(16).slice(2);
}

function findUser(array, paramObj) {
  const user = array.find((obj) => {
    for (let key in paramObj) {
      if (obj[key] === undefined || obj[key] != paramObj[key]) {
        return false;
      }
    }
    return true;
  })

  return user;
}

function filterArray(array, paramObj) {

  const filteredArray = array.filter((obj) => {
    for (let key in paramObj) {
      if (obj[key] === undefined || obj[key] !== paramObj[key]) {
        return false;
      }
    }
    return true;
  })

  return filteredArray;
}

function validateObject(object) {
  const errors = {
    full_name: validateString(object.full_name),
    gender: validateString(object.gender),
    note:  validateString(object.note),
    state: validateString(object.state),
    city: validateString(object.city),
    country: validateString(object.country),
    age: validateAge(object.age),
    phone: validatePhone(object.phone),
    email: validateMail(object.email)
  };

  console.log(JSON.stringify(errors));

  if (Object.values(errors).every(item => item === true)) {
    return true;
  }

  return 'Object is not valid. ' + JSON.stringify(errors);
}

const email_reg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const phone_reg = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/; // +(123)-456-78-90

const validateString = (str) => {
  if(!str) return false;
  return typeof str === 'string' && str.charAt(0) === str.charAt(0).toUpperCase();
}

const validateAge = (age) => {
  if(!age) return false;
  return typeof age === 'number';
}

const validatePhone = (phone) => {
  if(!phone) return false;
  return phone_reg.test(phone);
}

const validateMail = (mail) => {
  if(!mail) return false;
  return email_reg.test(mail);
}

function findUser(array, paramObj) {
  const user = array.find((obj) => {
    for (let key in paramObj) {
      if (obj[key] === undefined || obj[key] != paramObj[key]) {
        return false;
      }
    }
    return true;
  })

  return user;
}

function sortArray(array, param, order) {
  switch(param) {
    case 'full_name':
      if(order === 'decrease') {
        return array.sort((a, b) => (a.full_name.toLowerCase() < b.full_name.toLowerCase()) ? 1 : -1);
      }
      return array.sort((a, b) => (a.full_name.toLowerCase() > b.full_name.toLowerCase()) ? 1 : -1);
    case 'country':
      if(order === 'decrease') {
        return array.sort((a, b) => (a.country.toLowerCase() < b.country.toLowerCase()) ? 1 : -1);
      }
      return array.sort((a, b) => (a.country.toLowerCase() > b.country.toLowerCase()) ? 1 : -1);
    case 'age':
      if(order === 'decrease') {
        return array.sort((a, b) => (b.age - a.age));
      }
      return array.sort((a, b) => (a.age - b.age));
    case 'b_day':
      if(order === 'decrease') {
        return array.sort((a, b) => new Date(b.b_date) - new Date(a.b_date));
      }
      return array.sort((a, b) => new Date(a.b_date) - new Date(b.b_date));
    default:
      return array;
  }
}
